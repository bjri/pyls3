[TOC]



## Installation

PyLS3 is a python script. So python and all required packages need to be installed.

Currently, tested with Windows 10 and Linux, Python 3.7.9 and Python 3.8 (may work with other versions)

Here is a step by step installation guide

1. ##  Install python  

   Python could be downloaded from www.python.org.  
   Download the installer and follow the installation instructions.

2. ## Install the all required python packages  

   Open a Command line terminal (e.g. cmd, or xterm, ...)  
   Change into the PyLS3, e.g. `cd C:\Users\myuser\PyLS3`  
   Run pip to install the python packages `pip install -r requirements.txt`

3. ##  Now the PyLS3_UserCfg.yml configuration file must be edited to match your LS3 

   1. ### Modify the 'Device' section

      In this section all data for all your LS3 could be entered.  
      If you only have one LS3 you could remove LS3_2, LS3_3 ... if you have more devices you could add more devices...

      ```PyLS3_UserCfg.yml
      Device:
          LS3_1:							# Could be any name, the name in the 'Device' section and in the 'UseDevices' must match
              Model: LineScale3			# 
              Version: 2.600              # Enter the firmeware version of your LS (without v) 2.600 2.530 2.520 2.411
              MAC: 'F1:F2:F3:F4:F5:01'	# The Bluetooth mac of your LS3
              USB: COM8                   # Windows:COMx Linux:/dev/ttyUSBx - Cable must be connected to LS3 slave
              USB_Speed: 460800           # 9600 38400 230400 460800 (if not set 230400 is used) (Speed here and on LS3 'UART Baud' must match) (for 1280Hz 460800 should be used, or data is lost)
              TimingCorrectionFactor: 0.9 # correct timing inaccuracy (default 0.9)
          LS3_2:
              Model: LineScale3
              Version: 2.600
              MAC: 'F1:F2:F3:F4:F5:02'
              USB: COM9                    # Windows:COMx Linux:/dev/ttyUSBx - Cable must be connected to LS3 slave
              USB_Speed: 460800            # 9600 38400 230400 460800 (if not set 230400 is used) (Speed here and on LS3 'UART Baud' must match) (for 1280Hz 460800 should be used, or data is lost)
              TimingCorrectionFactor: 0.9  # correct timing inaccuracy (default 0.9)
      ```

      Name: The names (LS3_1, LS3_2) of the LS3 could be changed to any name. But the name in the 'Device' section and in the 'UseDevices' must match 

      Model: currently the model value is not used (best leave unchanged 'LineScale3')

      Version:  Enter the firmware version of your LS (without v). Is shown on LS3 if you push the Menu button

      MAC: Enter The Bluetooth mac of your LS3. Could be found under 'Menu->Service->Bluetooth Mac addr' on the LS3

      USB: Here you have to enter the USB serial Com-Port of your PC, if you use the USB serial connection.  
      The LS3 must be connected via a standard USB cable to the LS3 slave port (do not use the adapter for firmware upgrades).  
      If you do not know the USB serial Com-Port, PyLS3 will show all found USB serial Com-Ports (if ConnectionType is USB) - Serial-Port List '[...]'

      USB_Speed: Enter the speed of the USB serial Com connection.  
      The speed in the configuration and on LS3 'Menu->User Settings->UART Baud' must match.  
      If the Log-/Scan rate is set to 1280Hz the USB_Speed should be set to 460800, else logging-data will be lost on the  USB serial Com connection.

      TimingCorrectionFactor: The timing of the LS3 devices varies a little. This only has an effect to the onboard logging data.  
      to be able to plot graphs of multible LS3 PyLS3_multiplot.py uses the TimingCorrectionFactor to correct this deviation.  
      (there is a seperate section 'Timing / Rate inaccuracy' in the main README.md)

   2. ### Modify the 'UseDevices' section

      In this section you configure which LS3 devices, via which connection should be used by PyLS3.py.

      ```PyLS3_UserCfg.yml
      UseDevices:
          LS3_1:
              ConnectionType: Bluetooth            # USB Bluetooth none
              InitialCommands:                     # Capturing is activated after all Initial commands have been sent
                  - SaveOnboardLogging
                  - ModeABS
                  - UnitSwitchTokN
                  - Speed40
                  - Speed1280
                  #- ZeroButton
          LS3_2:
              ConnectionType: USB                  # USB Bluetooth none
              InitialCommands:
                  - SaveOnboardLogging
                  - ModeABS
                  #- ModeREL
                  #- ResetABS
                  - UnitSwitchTokN
                  #- UnitSwitchTokgf
                  #- UnitSwitchTolbf
                  #- Speed10
                  - Speed40
                  #- Speed640
                  - Speed1280
                  #- ForceClose
                  #- ZeroButton
      ```

      Name: The names (LS3_1, LS3_2) of the LS3 could be changed to any name. But the name in the 'Device' section and in the 'UseDevices' must match 

      ConnectionType: Could be set to 'USB' than the USB serial Com connection is used.  
      Or to 'Bluetooth', than the Bluetooth connection is used.  
      Or to 'none', than this device is skipped.

      InitialCommands: Here you could enter list of commands, which PyLS3.py will execute when the connection to the LS3 is established.
      Note, some commands are not supported via Bluetooth and will be skipped if connected via Bluetooth (e.g. SaveOnboardLogging, Speed640, Speed1280).
      All supported commands could be found in 'PyLS3_AppCfg.yml'->Commands  
      (The 'ForceClose' Command not always works, you may need to close the script by hand)

   3. ### Modify the 'Capture' section:

      Under this section, you could configure the PyLS3.py capture triggers and times.
      (this is completely independent of the onboard logging settings of the LS3)

      ```PyLS3_UserCfg.yml
      Capture:
          PreCaptureTime_s: 5                      # seconds in int
          StartTrigger: 0.04                       # value in load of SetUnit (to always capture set a low value e.g. -50.0, to not use StartTrigger use a high value e.g. 50.0)
          StopTrigger: -0.1                        # value in load of SetUnit (to deactivate set a low value e.g. -50.0)
          MaxCaptureTime_s: 60                     # seconds in int (seconds since StartTrigger)
          MinCaptureTime_s: 10                     # seconds in int  (seconds since StartTrigger)
          CaptureMode: continuous                  # continuous single
          TriggerMode: single                      # single (todo: combined)
          AutoGeneratePlot: True                   # True False
      ```

   4. ### Modify the 'OnboardLogging 'section

      The scripts need to know, which date format the LS3 onboard logging uses.
      LS3 Menu->Date/Time->Date format has two options 'dd.mm.yy' and 'yy/mm/dd'

      By default all PyLS3 script use 'dd.mm.yy', if another date formate is used it must be configured here.

      Default for 'dd.mm.yy' -  CSVDateFormatFromLS3:  '%d.%m.%y' (does not have to be configured)

      For ''yy/mm/dd'' -  CSVDateFormatFromLS3: '%y\%m\%d'

   ```PyLS3_UserCfg.yml
   OnboardLogging:
       CSVDateFormatFromLS3: '%y\%m\%d'        # must match the LS3 Setting (Menu->Date/Time->Date format) Available: '%d.%m.%y'(default) or '%y\%m\%d'
   ```

##  run PyLS3

Know you should be ready to run the PyLS3 scripts:

`PyLS3.py` or `python PyLS3.py` or `python3 PyLS3.py` 